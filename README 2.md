# HelloWAC-Reborn
## Mise en contexte
Le but de se projet est de créer une plateforme contactant les APIs de l'intranet Epitech afin de récupérer le trombinoscope de nos étudiants, diverses statistiques ainsi que la liste de nos projets et modules.

Plusieurs bonus pourront être implémentés tel que la possibilité de grader un module/projet, noter un étudiant ou encore récupérer le planning d'une promotion si les APIs de l'intranet le permettent.
### Détails administratifs
Vous possédez normalement les droits sur le répertoire suivant: [HelloWAC-Reborn]('//AdresseDuRepertoire').
Si vous ne pouvez pas accéder à ce répertoire n'hésitez pas à contacter votre responsable administratif qui se chargera de vous mettre les droits d'accès au répertoire.

Pour ce qui est du rendu, un push sur la branche Develop sera demandée chaque dimanche soir afin de procéder à une revue de code chaque lundi.
Cette revue de code entraînera un sprint meeting le lundi dans l'après-midi. Ce dernier permettra un retour sur la revue de code effectuée par votre responsable pédagogique dans la matinée ainsi que définir des objectifs claires pour la semaine d'après.

Le projet sera découpé en sprint d'un mois chacun. Un sprint comprendra un sprint planning le premier lundi permettant le récapitulatif des objectifs du sprint ainsi que l'organisation de celui-ci. Celui-ci sera suivi chaque lundi d'un sprint meeting dont les spécificités on été expliquées plus haut. Enfin ces sprints se termineront par une sprint review le dernier vendredi du mois durant entre deux et quatre heures suivant les fonctionnalités implémentées au cours du sprint.

Durant chacun de ses rendez-vous, votre responsable pédagogique jouera le rôle de Product Owner (ce qu'il représente car nous avons pour but d'utiliser la solution que vous allez produire) et vous aurez le rôle d'une équipe de développeur en condition réelle d'entreprise.
## Utilisation du Git
Avant de nous plonger totalement dans les fonctionnalités de la solution voulue, nous allons parler de l'utilisation du Git qui vous sera fourni pour ce projet.
Deux branches seront à votre disposition:
 - **Master**: branche de release, une fois la branche Develop validée durant une revue de sprint (dernier vendredi du mois), il sera attendu un **merge** de la branche Develop dans la branche Master.
 - **Develop**: branche de développement. Chaque branche, utilisée pour coder une fonctionnalité, sera crée depuis cette branche sous le model **Develop-NomDeLaFonctionnalite**
## Technologies
Pour ce projet Il a été choisi de partir sur une stack **MERN (MongoDB, Express, React Native, Node.js)**
Le serveur du projet sera contenu dans le dossier serverExpress à la racine du répertoire.
Le client de la solution sera contenu dans le dossier clientReactNative, lui aussi, à la racine du répertoire.
Pour le moment, la pérennité des données n'est pas envisagée. En effet, les données dont nous avons besoin sont déjà stockée dans les base de données d'EPITECH. Une base de données MongoDB sera mise en place afin de permettre une transition plus aisée si la pérennité devient obligatoire.
### Getting started
#### Node
Votre but premier sera de posséder npm. Pour ce faire nous allons devoir installer nodejs:
```
$ sudo apt-get update
```
```
$ sudo apt-get install nodejs
```
Veillez à vérifier la version de nodejs:
```
$ nodejs --version
8.1*.*
```
Il faudra ensuite installer npm:
```
$ sudo apt-get install npm
```
Veillez à vérifier la version de npm. Celle-ci peut-être la version 6.4 ou 6.5.
```
$ npm --version
6.4.*
```
#### MongoDB
Pour l'installation de MongoDB, il vous faudra lier la clé GPG de ce répertoire à votre APT. Ensuite, vous devrez créer un fichier contenant la source de ce répertoire. Vous allez pouvoir effectuer une update de votre APT. Enfin, vous pourrez installer la dernière version du paquet mongodb-org contenant les outils permettant d'utiliser mongoDB.
Tout cela se traduit par les quatre commandes suivantes:
```
$ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
$ echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
$ sudo apt-get update
$ sudo apt-get install -y mongodb-org
```
#### React Native
```
$ npm install react-native --save
```
Une autre solution existante pour installer react-native est de se mettre à la racine de clientReactNative et de lancer la commande *npm install*
#### Express
```
$ npm install express --save 
```
Une autre solution existante pour installer express est de se mettre à la racine de serverExpress et de lancer la commande *npm install*
## Sprint 1 - Launch
Ce premier sprint possède quatre objectifs principaux:
- Lancer la solution sur votre mobile.
- Mise en place du squelette front.
- Authentification via Microsoft 365 API.
- Récupération des étudiants depuis les APIs de l'intranet.
### Lancer sur mobile
Nous utilisons React native afin de posséder une version web et mobile de notre solution. Naturellement la première chose que vous ferez une fois le projet posséder sera de Build ce dernier afin d'y accéder depuis un navigateur et depuis votre mobile.
### Squelette
Le squelette correspond ici à l'agencement des diverses pages HTML.
Nous posséderons trois pages en ce début de projet:
- Home: la page d'accueil qui sera composée d'une barre de navigation permettant l'accès à toutes les autres pages du site. Cette dernière se devra d'être accessible uniquement si l'utilisateur est connecté.
- Login: la page de login contiendra un formulaire contenant **email**, **password** ainsi qu'un bouton submit. A l'opposé de ce formulaire, nous aurons un bouton "Se connecter avec Office 365" qui nous redirigera vers la page de connexion Office 365 de microsoft.
- Students: La page students contiendra un formulaire de recherche permettant la recherche par promotion, nom, école et ville des étudiants. Ces derniers seront représentés par un système de cartes contenant la photo de l'étudiant ainsi que son nom.
### Authentification
L'authentification nécessitera une prise en main des API microsoft. Le but sera d'utiliser ces dernières pour se connecter comme via l'intranet d'Epitech. Un enregistrement de la session avec un rafraîchissement possible du token est attendu.
[Utiliser l'API Rest Microsoft](https://docs.microsoft.com/en-us/previous-versions/office/office-365-api/api/version-2.0/use-outlook-rest-api)
Le lien dessus vous proposera deux manières de vous authentifier, aucune préférence n'est de mise de notre côté.
Un model User sera attendu côté Express. Pas de stockage de l'email && du mot de passe. A vous de définir les autres particularités du model.
### Contacter l'API Epitech
Cette partie sera sans doute la partie la plus intéressante. Nous ne possédons pas de documentation pour accéder aux diverses routes de l'API Epitech.
Votre but pour ce premier sprint va être de récupérer la liste des étudiants suivant les paramètres rentrées dans le formulaire de recherche de la page Students.
Pour vous aider dans cette quête nous ne pouvons que vous donner cette fonction de l'ancienne Hellowac qui permettait de récupérer ses fameux étudiants sous format JSON.
```
function  getStudentsFromEpitech($location, $scolaryear, $school, $semester, $course) {
	$wrapper = new  CurlWrapper($app['cookie']);
/* 	
 *	Exemple requête récupération des étudiants 
 *	https://intra.epitech.eu/admin/promo/list?
 *		&school=webacademie
 *		&scolaryear=2015
 *		&course=webacademie
 *		&semester=W1
 *		&location=FR/LYN
 *		&format=json
 */
	$students = json_decode($wrapper->get('https://intra.epitech.eu/admin/promo/list?'  .
		'&location='  .  $location  .
		'&scolaryear='  .  $scolaryear  .
		'&school='  .  $school  .
		'&course='  .  $course  .
		'&semester='  .  $semester  .
		'&format=json'
	));
	return  $students;
}
$app->get('/students/{country}/{town}/{scolaryear}/{id_school}/{semester}', function ($country, $town, $scolaryear, $id_school, $semester) use ($app) {
	$wrapper = new  CurlWrapper($app['cookie']);
	$school_data = getSchoolData($app, $id_school);
	$students = getStudentsFromEpitech($country  .  '/'  .  $town, $scolaryear, $school_data['slug'], $semester, $school_data['course']);
	foreach ($students as $student)
	{
		$student->netsoul = json_decode(get_netsoul($wrapper, $student->login));
		$student->notes = json_decode(get_notes($wrapper, $student->login));
		$student->student = json_decode(get_student($wrapper, $student->login));
	}
	return  new  Response(json_encode($students), 200);
});

function  get_student ($wrapper, $login) {
	return  $wrapper->get('https://intra.epitech.eu/user/' . $login . '/?format=json');
}
```
### Ouverture 
Le but du prochain sprint sera de mettre en place chaque Model dont nous aurons besoin (School, Town, Country, Students, ...) afin d'assurer une pérennité des données si celles-ci est voulue et nous donner des informations utilitaires tels que les listes des pays, villes et écoles dont nous devons récupérer les informations.